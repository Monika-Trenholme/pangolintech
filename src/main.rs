use axum::{
    body::Body,
    BoxError,
    extract::{Host, Request, State},
    handler::HandlerWithoutStateExt,
    http::uri::Uri, response::{IntoResponse, Redirect, Response},
    routing::any,
    Router
};
use hyper::{StatusCode, Version};
use hyper_util::{client::legacy::connect::HttpConnector, rt::TokioExecutor};
use std::net::{IpAddr, SocketAddr};

mod error;
mod macros;
mod pages;
mod root;

type Client = hyper_util::client::legacy::Client<HttpConnector, Body>;

const LOCAL_IP: [u8; 4] = [127, 0, 0, 1];
const PUBLIC_IP: [u8; 4] = [0, 0, 0, 0];
const ROOT_PORT: u16 = 8000;

#[cfg(debug_assertions)]
const HTTPS_PORT: u16 = 3000;
#[cfg(not(debug_assertions))]
const HTTPS_PORT: u16 = 443;
#[cfg(debug_assertions)]
const HTTP_PORT: u16 = 4000;
#[cfg(not(debug_assertions))]
const HTTP_PORT: u16 = 80;

macro_rules! proxy_helper {
    (
        $debug_domain:literal,
        $release_domain:literal,
        $($subdomain:literal => $subdomain_port:literal),*
    ) => {
        #[cfg(debug_assertions)]
        const BASE_HOST: &'static str = $debug_domain;
        #[cfg(not(debug_assertions))]
        const BASE_HOST: &'static str = $release_domain;

        #[cfg(not(debug_assertions))]
        const DOMAINS: &'static [&'static str] = &[
            BASE_HOST,
            $(concat!($subdomain, '.', $release_domain)),*
        ];

        fn domain_port(domain: &str) -> Result<u16, StatusCode> {
            match domain {
                BASE_HOST => Ok(ROOT_PORT),
                $(
                    #[cfg(debug_assertions)]
                    concat!($subdomain, '.', $debug_domain) => Ok($subdomain_port),
                    #[cfg(not(debug_assertions))]
                    concat!($subdomain, '.', $release_domain) => Ok($subdomain_port),
                )*
                _ => return Err(StatusCode::NOT_FOUND)
            }
        }
    };
}

proxy_helper! {
    "pangolintech.local:3000",
    "pangolintech.ca",
    "pei" => 8001
}

#[tokio::main]
async fn main() {
    let client: Client =
        hyper_util::client::legacy::Client::<(), ()>::builder(TokioExecutor::new())
            .build(HttpConnector::new());
    let app = Router::new()
        .route("/", any(proxy_handler))
        .route("/*path", any(proxy_handler))
        .with_state(client)
        .into_make_service();
    tokio::spawn(redirect_http_to_https());
    tokio::spawn(async {
        let address = SocketAddr::from((LOCAL_IP, ROOT_PORT));
        let listener = tokio::net::TcpListener::bind(address).await.unwrap();
        axum::serve(listener, root::router()).await.unwrap();
    });

    let address = SocketAddr::from((PUBLIC_IP, HTTPS_PORT));
    // Use simple HTTP setup in debug
    #[cfg(debug_assertions)]
    {
        let listener = tokio::net::TcpListener::bind(address).await.unwrap();
        axum::serve(listener, app).await.unwrap();
    }
    #[cfg(not(debug_assertions))]
    {
        use rustls_acme::{AcmeConfig, caches::DirCache};
        use tokio_stream::StreamExt;
        let mut state = AcmeConfig::new(DOMAINS)
            .contact(["mailto:monika-trenholme@protonmail.com"])
            .cache(DirCache::new("ssl_cache"))
            .directory_lets_encrypt(true)
            .state();
        let acceptor = state.axum_acceptor(state.default_rustls_config());
        tokio::spawn(async move {
            loop {
                match state.next().await.unwrap() {
                    Ok(ok) => print!("event: {ok:?}"),
                    Err(err) => eprintln!("error: {err:?}"),
                }
            }
        });
        axum_server::bind(address).acceptor(acceptor).serve(app).await.unwrap();
    }
}

async fn proxy_handler(Host(host): Host, State(client): State<Client>, mut req: Request) -> Result<Response, StatusCode> {
    let path = req.uri().path();
    let path_query = req
        .uri()
        .path_and_query()
        .map(|v| v.as_str())
        .unwrap_or(path);

    let address = IpAddr::from(LOCAL_IP);
    let port = domain_port(&host.to_lowercase())?;
    *req.uri_mut() = Uri::try_from(format!("http://{address}:{port}{path_query}")).unwrap();
    // For some reason there is an issue proxying with HTTP_2
    *req.version_mut() = Version::HTTP_11;

    match client.request(req).await {
        Err(e) => {
            eprintln!("{e:?}");
            Err(StatusCode::BAD_REQUEST)
        }
        Ok(res) => Ok(res.into_response())
    }
}

async fn redirect_http_to_https() {
    fn make_https(host: String, uri: Uri) -> Result<Uri, BoxError> {
        let mut parts = uri.into_parts();

        parts.scheme = Some(axum::http::uri::Scheme::HTTPS);

        if parts.path_and_query.is_none() {
            parts.path_and_query = Some("/".parse().unwrap());
        }

        let https_host = host.replace(&HTTP_PORT.to_string(), &HTTPS_PORT.to_string());
        parts.authority = Some(https_host.parse()?);

        Ok(Uri::from_parts(parts)?)
    }

    let redirect = move |Host(host): Host, uri: Uri| async move {
        match make_https(host, uri) {
            Ok(uri) => Ok(Redirect::permanent(&uri.to_string())),
            Err(error) => {
                eprint!("Failed to convert URI to HTTPS {error:?}");
                Err(StatusCode::BAD_REQUEST)
            }
        }
    };

    let address = SocketAddr::from((PUBLIC_IP, HTTP_PORT));
    let listener = tokio::net::TcpListener::bind(address).await.unwrap();
    axum::serve(listener, redirect.into_make_service()).await.unwrap();
}