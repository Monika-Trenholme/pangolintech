use crate::{macros::{page, router}, pages::*};

router! {
    "/assets" => dir("assets"),
    "/media" => dir("media"),
    "/" => method(page!("home.html")),
    "/philosophy" => method(page!("philosophy.html")),
    "/music/chumbawamba" => nest(chumbawamba)
}