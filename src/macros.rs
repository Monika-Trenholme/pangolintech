macro_rules! router {
    ($($route:literal => $route_type:ident($($route_args:tt)*)),*) => {
        pub fn router() -> axum::Router {
            let router = axum::Router::new();
            $(let router = router!(register(router, $route, $route_type($($route_args)*)));)*
            router
        }
    };
    (register($router:ident, $route:literal, dir($path:literal))) => {
        $router.nest_service($route, tower_http::services::ServeDir::new($path))
    };
    (register($router:ident, $route:literal, method($method:expr))) => {
        $router.route($route, $method)
    };
    (register($router:ident, $route:literal, nest($module:ident))) => {
        $router.nest($route, $module::router())
    };
}

macro_rules! page {
    ($path:literal) => {
        page! {
            $path
            Page {}
            ()
            Ok(Self {})
        }
    };
    ($path:literal $($struct_name:ident { $($struct_body:tt)* })* ($($params:tt)*) $($code:tt)*) =>{
        axum::routing::get({
            $(page!($path struct $struct_name { $($struct_body)* });)*

            impl Page {
                async fn new($($params)*) -> crate::error::Result<Self> {
                    $($code)*
                }
            }

            Page::new
        })
    };
    ($path:literal struct Page { $($body:tt)* }) => {
        #[derive(askama_axum::Template)]
        #[template(path = $path)]
        struct Page { $($body)* }
    };
}

pub(crate) use router;
pub(crate) use page;