use crate::{error::{Result, status}, macros::{page, router}};
use axum::extract::Path;
use tokio::fs;
use serde::Deserialize;

const MEDIA_ROOT: &'static str = "media/music/chumbawamba";

#[derive(Deserialize)]
struct Album {
    title: String,
    songs: Vec<String>
}

async fn get_albums() -> Result<Vec<Album>> {
    let str = &fs::read_to_string(format!("{MEDIA_ROOT}/albums.json")).await?;
    Ok(serde_json::from_str(str)?)
}

router! {
    "/" => method(page! {
        "chumbawamba/home.html"
        
        Page {
            album_titles: Vec<String>
        }

        ()
        let album_titles = get_albums().await?
            .into_iter()
            .map(|album| album.title)
            .collect();
        Ok(Page { album_titles })
    }),
    "/:album" => method(page! {
        "chumbawamba/album.html"

        Page {
            title: String,
            songs: Vec<String>
        }

        (Path(album_title): Path<String>)
        let album = get_albums().await?
            .into_iter()
            .find(|album| album.title == album_title)
            .ok_or(status!(NOT_FOUND))?;
        Ok(Page {
            title: album_title,
            songs: album.songs,
        })
    }),
    "/:album/:song" => method(page! {
        "chumbawamba/song.html"

        Page {
            title: String,
            album_title: String
        }

        (Path((album_title, song_title)): Path<(String, String)>)
        let album = get_albums().await?
            .into_iter()
            .find(|album| album.title == album_title)
            .ok_or(status!(NOT_FOUND))?;
        let song = album.songs
            .into_iter()
            .find(|title| title == &song_title)
            .ok_or(status!(NOT_FOUND))?;
        Ok(Page { title: song, album_title })
    })
}