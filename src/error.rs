use axum::{http::StatusCode, response::{IntoResponse, Response}};
use askama_axum::Template;

pub enum Error {
    Anyhow(anyhow::Error),
    Status(StatusCode)
}
pub type Result<T> = std::result::Result<T, Error>;

#[derive(Template)]
#[template(path = "error.html")]
struct ErrorPage {
    status_code: StatusCode,
    info: Option<String>
}

macro_rules! status {
    ($status_code:ident) => {
        crate::error::Error::Status(axum::http::StatusCode::$status_code)
    };
}

pub(crate) use status;

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        let (status_code, info) = match self {
            Self::Anyhow(error) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                Some(format!("Error: {error}"))
            ),
            Self::Status(status_code) => (status_code, None)
        };
        (status_code, ErrorPage { status_code, info }).into_response()
    }
}

impl<E> From<E> for Error
where
    E: Into<anyhow::Error>,
{
    fn from(err: E) -> Self {
        Self::Anyhow(err.into())
    }
}